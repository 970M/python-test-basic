#
# LISTE CHAINEE
#

#===================================================================================
# CLASS
#===================================================================================
class Cell:
    """Une cellule d'une liste chainee"""

    def __init__(self, v, s):
        self.value = v  # type pas precise
        self.next = s   # la prochaine cellule

class List:
    """Une liste chainee"""

    def __init__(self):
        self.head = None

    # Retourner True si la liste est vide
    def is_empty(self):
        return self.head is None

    # Ajouter une cellule à la fin de la liste
    def add_cell(self, v):
        self.head = Cell(v, self.head)
    
    # Renvoyer la longueur de la liste
    def length(self):
        return lst_length(self.head)

    # Trier la liste
    def sorted(self):
        self.head = lst_tri_select(self.head)

    def get_cell(self, n):
        """Renvoi le n ieme element d'une liste chainee, numerote a partir de 0"""
        return lst_get_cell(self.head, n)

    def switch(self, i, j):
        self.head = lst_exchange(self.head, i, j)

    def insert(self, i, v):
        self.head = lst_insert(self.head, i, v)

    def delete(self, i):
        self.head = lst_delete(self.head, i)

    def reverse(self):
        self.head = lst_reverse(self.head)

    def add(self, lsth):
        self.head = lst_concatenate_rec(self.head, lsth.head)

#===================================================================================
# Fonction sur pointeur de cellule (.head)
#===================================================================================
def lst_length(lsth):
    """Longeur d'une liste"""
    len = 0
    c = lsth
    # print("c=",c.next.value)
    while c is not None:
        c = c.next
        len += 1
    return len

def lst_split(lsth, i):
    """Sépare une liste en 2 à l'indice i"""
    n = 0
    h1 = None
    h2 = lsth
    # On met dans h1 tout ce qu'il y a avant i
    while (h2 is not None) and (n <= i): 
        h1 = Cell(h2.value, h1)
        h2 = h2.next
        n += 1
    return lst_reverse(h1), h2

def lst_concatenate_rec(lsth1, lsth2):
    """Concatene ltsh2 apres lsth1 et retourne le tout"""
    h = None
    c = lsth1
    if c is None:
        return lsth2
    else:
        # Ajouter une cellule dans .next recursivement
        h = Cell(c.value, lst_concatenate_rec(c.next, lsth2))
    return h

def lst_concatenate_ite(lsth1, lsth2):
    """Concatene ltsh2 apres lsth1 et retourne le tout"""
    if lsth1 is None:
        return lsth2
    else:
        h = lsth2
        c = lst_reverse(lsth1)
        while c is not None:
            h = Cell(c.value, h)
            c = c.next
        return h

def lst_insert(lsth, i, v):
    """Insere la valeur v a la position i"""
    h = None
    c = lsth
    n = 0
    # On parcours la liste et en i on place
    # la nouvelle cellule avant la cellule i existante
    while c is not None:
        if n == i:
            h = Cell(v, h)
            h = Cell(c.value, h)
        else:
            h = Cell(c.value, h)
        c = c.next
        n += 1
    return lst_reverse(h)

def lst_delete(lsth, i):
    """Supprime la cellule i"""
    h = None
    c = lsth
    n = 0
    while c is not None:
        if n != i:
            h = Cell(c.value, h)
        c = c.next
        n += 1
    return lst_reverse(h)

def lst_get_cell(lsth, i):
    """Renvoi la valeur de la cellule i"""
    n = 0
    c = lsth
    if i < 0:
        raise IndexError("indice invalide")
    while (c is not None) and (n < i):
        # print("ind=",ind)
        c = c.next
        n += 1
    return c.value

def lst_reverse(lsth):
    """Inverse l'ordre des element d'un liste"""
    r = None
    c = lsth
    n = 0
    while c is not None:
        r = Cell(c.value, r)
        c = c.next
    return r

def lst_exchange(lsth, i, j):
    """ Echange la position de la cellule i avec celle de j"""
    if i != j:
        r = None
        c = lsth
        n = 0
        while c is not None:
            if n == i: # On recherche la valeur de la cellule j pour la placer en i
                r = Cell(lst_get_cell(lsth, j), r)
            elif n == j: # On recherche la valeur de la cellule i pour la placer en j
                r = Cell(lst_get_cell(lsth, i), r)
            else: # On laisse tel quel
                r = Cell(c.value, r)
            c = c.next
            n += 1
        return lst_reverse(r)
    else:
        return lsth