import list_lib

"""
https://ichi.pro/fr/tests-unitaires-en-python-les-bases-37012509038752
https://docs.python.org/fr/3/library/unittest.html
https://gayerie.dev/docs/python/python3/unittest.html
python -m unittest discover -p "*.py" > results.txt
https://www.codeflow.site/fr/article/python-testing
https://www.codeflow.site/fr/article/python-testing#_more_advanced_testing_scenarios
https://docs.pytest.org/en/6.2.x/
"""


def test_lst_is_empty():
    
    input = True
    output = list_lib.List().is_empty()
    assert output == input
    

def test_add_cell():
    
    output = list_lib.List()
    output.add_cell(40)
    
    output.add_cell('txt')

    assert output.head.next.value == 40
    assert output.head.value == 'txt'


if __name__ == '__main__':
    test_lst_is_empty()
    test_add_cell()
    